# Zebra Crossing

<img src="https://gitlab.com/petrahugyecz/zebra-crossing/uploads/f928c3f995b858b47a147c8730c2ac35/zebra_app_icon_2020.jpg" width="200" height="200" /><br>

<img src="https://gitlab.com/petrahugyecz/zebra-crossing/uploads/e2d9fa63a8ed68881c77d3053964f0e2/1.png" width="200" height="400" />

<img src="https://gitlab.com/petrahugyecz/zebra-crossing/uploads/26e6a3210601d939c5eab2b149c782de/2.png" width="200" height="400" />

<br><br>
🦓 **Zebra Crossing** is a casual game where you need to walk through a city with traffic getting higher by each level. Cars, trucks, trains and boats will get in your way, not to mention city workers always watching you! Believe us, there will be consequences if you cross a red light or don't let them sweep the sidewalk :smiley:


Did you get in some trouble? No worries, you can run away, hide in the phone booth or use some handy power-ups to get as far as you can! Freezing time, turning invisible and getting out of a hot situation with a balloon is all possible in Zebra Crossing.


Pick your favorite character and outfit in our in-game store and don't forget to empty your constantly refilling piggy bank on your way!

🤖 **Android:** https://play.google.com/store/apps/details?id=com.SMALLthings.Zebra&hl=hu&gl=US <br>
🍏 **iOS:** https://apps.apple.com/us/app/zebra-crossing/id1573405273

